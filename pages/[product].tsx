import Image from 'next/image'
import { useRouter } from 'next/router'
import React from 'react'
import TextHeading from './Components/MicroComp/TextHeading'
import {motion} from 'framer-motion'
import Link from 'next/link'

export default function Product() {
    const {query : {product}}:any = useRouter()
    return (
        <div className='md:flex h-[700px]'>
            <div className="sm:w-6/12 w-9/12 mt-20  md:m-0 m-auto shadow-xl p-5">
            <motion.img
            src={`/img/${product}.jpg`} 
            alt='makanan'
            layoutId={product}
            className=' object-cover h-full w-full '
            />
            </div>
            <div className="sm:w-6/12 w-10/12 px-5 md:m-0 m-auto">
            <TextHeading H1={product}/> 
            <p className='font-body text-gray-800'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit dolor quasi laudantium maxime, aliquid magnam omnis architecto excepturi optio ad inventore accusantium nisi consectetur delectus voluptatibus, explicabo qui? Corrupti, odio!</p>
            </div>
        </div>
    )
}
