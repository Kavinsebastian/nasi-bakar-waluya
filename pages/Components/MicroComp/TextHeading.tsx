import React from 'react'

export default function TextHeading({H1}:any) {
    return (
        <div className=' text-green-900 h-52 font-bold sm:mt-20 flex justify-center items-center font-Heading text-3xl'>
            <h1>{H1}</h1>
        </div>
    )
}
