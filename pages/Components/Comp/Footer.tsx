import React from "react";
import Ig from '../../../public/img/icons8-instagram.svg'
import Fb from '../../../public/img/icons8-facebook.svg'
import Wa from '../../../public/img/icons8-whatsapp.svg'
import Image from "next/image";

export default function Footer() {
  return <section id='footer'>
    <div className="p-5 bg-gray-600 mt-5 h-auto">
      <div className="flex justify-around w-6/12 m-auto text-gray-200" id="social-media">
        <div className="hover:bg-gray-100 duration-300 hover:bg-opacity-20 p-2 rounded-full cursor-pointer">
        <Image src={Ig} alt="ig"/>
        </div>
        <div className="hover:bg-gray-200 duration-300 hover:bg-opacity-20 p-2 rounded-full cursor-pointer">
        <Image src={Fb} alt="ig"/>
        </div>
        <div className="hover:bg-gray-200 duration-300 hover:bg-opacity-20 p-2 rounded-full cursor-pointer">
        <Image src={Wa} alt="ig"/>
        </div>
      </div>
      <div className="text-center text-gray-400">
        <p>Privacy Statement | Terms of Use | site Map | Cookie Preverences</p>
        <text> &#169; 2021 Nasi Bakar Waluya. All rights riserved </text>
      </div>
      </div>
  </section>;
}
