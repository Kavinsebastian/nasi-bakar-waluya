import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import Image from "next/image";
import Link from "next/link";

export default function Navbar({ linkAbout, linkOrder }: any) {
  const [isOpen, setIsOpen] = useState(false);

  const [scrollPosition, setPosition] = useState(0);

  useEffect(() => {
    function updatePosition() {
      setPosition(window.pageYOffset);
    }
    window.addEventListener("scroll", updatePosition);
    updatePosition();
  }, []);

  return (
    <motion.div
      className={`${
        scrollPosition > 450 ? "fixed z-50 bg-gray-400 translate-y-20 transition-transform -top-20 duration-1000 border-0 shadow-md" : "border-y-2 bg-transparent"
      } w-full  border-black h-[80px] p-5`}>
      <ul className='flex items-center font-semibold font-sans'>
        <div className='w-3/12'>
          <li>
            <Link href='/' passHref>
              <Image src={"/img/logo.jpeg"} width={100} height={50} alt='logo' />
            </Link>
          </li>
        </div>
        <div className='w-6/12 text-center font-bold'>
          <li>
            <h1 className='font-Brand text-sm md:text-3xl'>Nasi Bakar Waluya</h1>
          </li>
        </div>
        <Link href='/' passHref>
          <li className='mx-2 border cursor-pointer px-5 sm:inline hidden py-2 hover:bg-black hover:text-white'>Home</li>
        </Link>
        <Link href={linkAbout ? linkAbout : '#about'} passHref>
          <li className='mx-2 border px-5 cursor-pointer sm:inline hidden py-2 hover:bg-black hover:text-white'>About</li>
        </Link>
        <Link href={linkOrder ? linkOrder : '#order'} passHref>
          <li className='mx-2 border px-5 cursor-pointer sm:inline hidden py-2 hover:bg-black hover:text-white'>Order</li>
        </Link>
        <Link href='/menu' passHref>
          <li className='mx-2 border px-5 cursor-pointer sm:inline hidden py-2 hover:bg-black hover:text-white'>Menu</li>
        </Link>
        <div className='sm:hidden'>
          <button onClick={() => setIsOpen(!isOpen)} type='button' className={`${isOpen ? "left-40" : "right-4"} absolute z-50 top-4`} aria-controls='mobile-menu' aria-expanded='false'>
            <motion.div
              whileHover={{ scale: 1.1 }}
              whileTap={{
                scale: 0.8,
                borderRadius: "100%",
              }}>
              <span className='sr-only'>Open main menu</span>
              {!isOpen ? (
                <div className=''>
                  <Image src={"/img/menu.png"} width={50} height={35} alt='menu' />
                </div>
              ) : (
                <div className=''>
                  <Image src={"/img/right-arrow.png"} width={35} height={35} alt='menu' />
                </div>
              )}
            </motion.div>
          </button>
        </div>

        <motion.div
          initial={false}
          animate={isOpen ? { right: 150 } : { right: 10000 }}
          transition={{ ease: "easeInOut", duration: 0.5 }}
          className={`bg-gray-700  absolute sm:hidden h-[100vh] w-full rounded-t-2xl  top-0 z-10`}>
          <ul className='text-center'>
            <Link href='/' passHref>
              <li className='hover:bg-gray-100 cursor-pointer hover:text-black text-white p-5 px-10  mt-28 rounded-sm'>Home</li>
            </Link>
            <Link href='#about' passHref>
              <li className='hover:bg-gray-100 cursor-pointer hover:text-black text-white p-5 px-10'>About</li>
            </Link>
            <Link href='#order' passHref>
              <li className='hover:bg-gray-100 cursor-pointer hover:text-black text-white p-5 px-10'>Order</li>
            </Link>
            <Link href='/menu' passHref>
              <li className='hover:bg-gray-100 cursor-pointer hover:text-black text-white p-5 px-10'>menu</li>
            </Link>
          </ul>
        </motion.div>
      </ul>
    </motion.div>
  );
}
