import React from "react";
import Head from "next/head";

export default function Heading() {
  return (
    <>
      <Head>
        {/* eslint-disable-next-line @next/next/no-page-custom-font */}
        <link href='https://fonts.googleapis.com/css2?family=El+Messiri:wght@700&family=Poppins&family=Saira:wght@500&display=swap' rel='stylesheet' />
        <link rel="icon" href="/img/logo.jpeg" />
        <title>Nasi Bakar Waluya</title>
      </Head>
    </>
  );
}
