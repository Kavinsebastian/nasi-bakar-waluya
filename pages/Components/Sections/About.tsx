import Image from "next/image";
import Link from "next/link";
import React from "react";
import Food from '../../../public/img/ayam.jpg'
import TextHeading from "../MicroComp/TextHeading";
import {motion} from 'framer-motion'

export default function About() {
  return (
    <section id='about' className='z-10 w-full bg-gray-50 h-auto lg:h-[780px] relative motion-safe:scroll-smooth'>
      <div className='lg:w-7/12 sm:w-9/12 text-center m-auto'>
        <TextHeading H1="Best Seller of Beverages"/>
        <div className='sm:p-5 object-cover justify-around lg:flex'>
          {
            ['Nasi Bakar Ayam Suir', 'Nasi Bakar Cumi-cumi'].map((product:any, key:any) => (
              <Link href={product} key={key} passHref>
          <div className='lg:w-5/12 shadow-md p-2 lg:m-0 m-10 duration-300 hover:scale-125 cursor-pointer bg-white'>
            <motion.img 
            src={`/img/${product}.jpg`} 
            alt='food'
            className="w-full h-96 object-cover"
            animate={{scale:1}}
            whileHover={{scale:1.1}}
            layoutId={product}
            />
            <h1 className="font-body text-sm text-gray-800 font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, consequatur.</h1>
          </div>
              </Link>
            ))
          }
        </div>
      </div>
    </section>
  );
}
