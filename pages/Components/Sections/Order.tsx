import React, { useRef } from "react";
import img1 from "../../../public/img/Nasi Bakar Ayam Suir.jpg";
import img2 from "../../../public/img/Nasi Bakar Cumi-cumi.jpg";
import img3 from "../../../public/img/Nasi Bakar Pindang.jpg";
import img4 from "../../../public/img/Sambel.jpg";
import img5 from "../../../public/img/Telor Ceplok.jpg";
import img6 from "../../../public/img/Telor Dadar.jpg";
import img7 from "../../../public/img/Es Blackcurrant.jpg";
import img8 from "../../../public/img/Es Lecy.jpg";
import img9 from "../../../public/img/Es Lemon Tea.jpg";
import img10 from "../../../public/img/Es Teh Manis.jpg";
import Image from "next/image";
import TextHeading from "../MicroComp/TextHeading"

export default function Order() {
  const {Scrollbars} = require('react-custom-scrollbars')
  return (
    <section id='order'>
      <TextHeading H1='Order' />
        <div className='relative block m-auto w-full h-[100vh] py-28 '>
      {/* <HorizontalScroll animValues={100}> */}
<div className="overflow-x-scroll">

          <div className='sticky   top-52 flex content-between items-center bg-white p-10' style={{ transformStyle: "preserve-3d", width: "300vh" }}>
            <div className='bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img1} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img2} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img3} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img4} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img5} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img6} alt='food' />
            </div>
            <div className=' bg-contain bg-center bg-no-repeat mx-5 w-2/5 shadow-2xl'>
              <Image src={img7} alt='food' />
            </div>
          </div>
</div>
      {/* </HorizontalScroll> */}
        </div>
    </section>
  );
}
