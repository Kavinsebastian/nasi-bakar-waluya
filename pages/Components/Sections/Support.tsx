import React from "react";
import TextHeading from "../MicroComp/TextHeading";
import Image from "next/image";
import Gojek from '../../../public/img/Logo GO FOOD HD.png'
import Grab from '../../../public/img/Logo Grab Food.png'
import Shopee from '../../../public/img/Logo Shopee Food.png'



export default function Support() {
  return <section id='support'>
    <div className=" bg-gray-200 h-[500px]">
      <TextHeading H1='Support'/>
    <div className="flex justify-evenly opacity-70">
      <Image src={Gojek} width={300} height={200} alt="gojek" />
      <Image src={Grab} width={300} height={200} alt="gojek" />
      <Image src={Shopee} width={300} height={200} alt="gojek" />
    </div>
    </div>
  </section>;
}
