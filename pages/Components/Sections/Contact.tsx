import React from "react";
import Iframe from "../MicroComp/Iframe";
import TextHeading from "../MicroComp/TextHeading";

export default function Contact() {
  const iframe =
    '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.7312213879013!2d107.63605871485817!3d-6.922699394998136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7d9707ef37f%3A0xbefdeb316b228d07!2sJl.%20Cangkring%20Raya%2C%20Maleer%2C%20Kec.%20Batununggal%2C%20Kota%20Bandung%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1642485870928!5m2!1sid!2sid" width="500px" height="400px" style="border:2;" allowfullscreen="" loading="lazy"></iframe>';

  const iframeMobile =
    '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.7312213879013!2d107.63605871485817!3d-6.922699394998136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7d9707ef37f%3A0xbefdeb316b228d07!2sJl.%20Cangkring%20Raya%2C%20Maleer%2C%20Kec.%20Batununggal%2C%20Kota%20Bandung%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1642485870928!5m2!1sid!2sid" width="300px" height="200px" style="border:2;" allowfullscreen="" loading="lazy"></iframe>';

  return (
    <div className='bg-white xl:h-[700px] mt-52 pb-10'>
      <TextHeading H1='Contact' />
      <div className='md:w-8/12 p-5 border-black md:border-2 m-auto xl:flex'>
        <div className='sm:inline hidden'>
          <Iframe iframe={iframe} />
        </div>
        <div className='sm:hidden inline'>
          <Iframe iframe={iframeMobile} />
        </div>
        <div className="w-full h-auto border-black md:border-l-2 my-5  md:m-5">
          <div className="text-center bg-blue-500 h-full md:mx-5 p-5 text-gray-200 font-body flex items-center justify-center font-bold">
            <div className="m-2">
          <h1 className="p-2">Nasi Bakar Waluya</h1>
          <p className="p-2">Jl. Cangkring Raya Kec. Batununggal Kota Bandung Jawa Barat</p>
          <p className="p-2">089555555524</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
