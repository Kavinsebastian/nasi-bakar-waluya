import React, { useState } from "react";
import Navbar from "./Components/Comp/Navbar";
import { motion } from "framer-motion";
import Link from "next/link";
import Footer from "./Components/Comp/Footer";
import Heading from "./Components/Comp/Heading";






export default function Menu() {
  const menuItem = ['Nasi Bakar Pindang', 'Nasi Bakar Ayam Suir', 'Nasi Bakar Cumi-cumi', 'Sambel', 'Telor Ceplok', 'Telor Dadar', 'Es Teh Manis', 'Es Blackcurrant', 'Es Lecy', 'Es Lemon Tea']
  const [search, setSearch] = useState([] as any)
  const [dataSearch, setDataSearch] = useState('')

  const handleChange = (e:any) => {
    setDataSearch(e.target.value)
  }


  const handleClick = () => {
    let result:any = []
    for(let i = 0; i < menuItem.length; i++){
      const item = menuItem[i].split(' ')
      for(let j = 0; j < item.length; j++){
        if(item[j].toLowerCase() === dataSearch.toLowerCase()){
          if(result.indexOf(item) === -1){
            result.push(menuItem[i])
          }
        }
      }
    }
    if(result.length > 0){
      setSearch(result)
    }else{
      setSearch(false)
    }
  }

  return <div>
    <Heading/>
    <Navbar linkAbout='/#about' linkOrder='/#order' />
    <div className="md:w-6/12 m-auto my-5 h-auto">
      <div className="md:p-5 p-2">
      <label htmlFor="">
        Menu <br />
        <input type="text" onChange={(e) => handleChange(e)}  className="px-5 py-2 rounded-sm bg-gray-300 focus:bg-gray-100"/>
        <button onClick={handleClick} className="px-5 py-2 bg-green-900 text-white rounded-sm mx-5 hover:bg-white border-2 hover:border-green-900 hover:text-green-900 font-body">Submit</button>
      </label>
        </div>
      
      {
        !search ? (
          <div className="h-[400px]">
            <h1>Menu yang anda cari tidak ada</h1>
          </div>
        )
        :
      <div className='sm:p-5 mt-20 object-cover flex flex-col md:grid md:grid-cols-2 md:gap-4'>
          {
            search.length < 1 ? 
            menuItem.map((product:any, key:any) => (
              <Link href={product} key={key} passHref>
          <div className=' shadow-md p-2 lg:m-0 m-10 duration-300 hover:scale-125 cursor-pointer bg-white'>
            <h1 className="font-body text-green-900 p-5 text-center font-bold">{product}</h1>
            <motion.img 
            src={`/img/${product}.jpg`} 
            alt='food'
            className=""
            animate={{scale:1}}
            whileHover={{scale:1.1}}
            layoutId={product}
            />
            <h1 className="p-5">Rp.9000</h1>
            <h1 className="font-body text-sm text-gray-800 font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, consequatur.</h1>
          </div>
              </Link>
            ))
            :
            search.map((product:any, key:any) => (
              <Link href={product} key={key} passHref>
          <div className=' shadow-md p-2 lg:m-0 m-10 duration-300 hover:scale-125 cursor-pointer bg-white'>
            <h1 className="font-body text-green-900 p-5 text-center font-bold">{product}</h1>
            <motion.img 
            src={`/img/${product}.jpg`} 
            alt='food'
            className=""
            animate={{scale:1}}
            whileHover={{scale:1.1}}
            layoutId={product}
            />
            <h1 className="p-5">Rp.9000</h1>
            <h1 className="font-body text-sm text-gray-800 font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, consequatur.</h1>
          </div>
              </Link>
            ))
          }
      </div>
        
      }
      
    </div>
    <Footer/>
  </div>;
}
