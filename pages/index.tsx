/* eslint-disable @next/next/no-page-custom-font */
/* eslint-disable react/jsx-no-comment-textnodes */
import type { NextPage } from "next";
import Head from "next/head";
import Footer from "./Components/Comp/Footer";
import Navbar from "./Components/Comp/Navbar";
import About from "./Components/Sections/About";
import Contact from "./Components/Sections/Contact";
import Order from "./Components/Sections/Order";
import Support from "./Components/Sections/Support";
// eslint-disable-next-line @next/next/no-document-import-in-page
import Heading from "./Components/Comp/Heading";


const Home: NextPage = () => {
  return (
    <>
    <Heading/>
      <div className='box-border scroll-smooth bg-bg-hero w-full h-auto -z-10 bg-cover bg-fixed'>
        <div id='header' className="h-[100vh] relative">
          <Navbar linkAbout='#about' linkOrder='#order' />
        </div>
        <div className='w-full h-2/4 absolute top-10 sm:top-32'>
          <div className='md:w-6/12 text-center mt-28 h-52 my-5 m-auto'>
            <h1 className='font-bold my-5 text-lg sm:text-2xl font-Heading'>Delicious Nusantara Foods</h1>
            <p className='mb-28 text-gray-800 font-body font-semibold'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio beatae eligendi quidem ipsa alias non fugit labore officiis maxime illo?</p>
            <button className="px-5 font-bold text-green-900 py-2 border-t-2 border-b-2 border-green-900 duration-700 ease-in-out hover:bg-green-900 hover:text-white">Grab Now</button>
          </div>
        </div>
        <About/>
        <Order/>
        <Support/>
        <Contact/>
        <Footer/>
      </div>
        
    </>
  );
};

export default Home;
