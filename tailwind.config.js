module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  variats:{
    extends:{
      scrollBehavior: ['motion-safe', 'motion-reduce', 'responsive'],
      scrollbar:['dark', 'rounded']
    }
  },
  theme: {
    extend: {
      backgroundImage:{
        'bg-hero':'url(../public/img/Hero.jpg)'
      },
      fontFamily:{
        "Brand":["El Messiri", "sans serif", "system-ui" ],
        "Heading":["Saira", "serif", "arial"],
        "body":["poppins", "serif","arial"]
      }
    },
    
  },
  plugins: [
    require('tailwind-scrollbar')
  ],
}
